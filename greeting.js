let currentDate = new Date();

global.date = currentDate;

module.exports.date = currentDate;
module.exports.getMessage = function (name) {
  let hour = currentDate.toLocaleTimeString('en-US', {
    hour: '2-digit',
  });
  console.log(hour);
  if (hour > 04) return 'Good evening, ' + global.name;
  else if (hour > 04 && hour < 12) return 'Good morning, ' + name;
  else return 'Good tidings, ' + name;
};
