const pidor = {
  name: 'Alex',
  sing() {
    console.log('a this', this);
    var anotherFunc = function () {
      console.log('b this', this);
    };
    anotherFunc();
  },
};

// pidor.sing();

govno = {};
Object.setPrototypeOf(govno, pidor);
govno.__proto__;
