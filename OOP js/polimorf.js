class Human {
  constructor(name) {
    this.name = name;
  }
  sayMyName() {
    return 'Hello, I am ' + this.name;
  }
}

class Men extends Human {
  constructor(name) {
    super(name);
  }
}

class Coder extends Human {
  constructor(name) {
    super(name);
  }
}

const alex = new Coder('Alex');
const leo = new Men('Leo');

alex.sayMyName();
leo.sayMyName();
