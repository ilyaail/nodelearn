const express = require('express');
const app = express();
const productRouter = express.Router();

productRouter.use('/create', function (request, response) {
  response.send('Add Items');
});
productRouter.use('/:id', function (request, response) {
  response.send(`Item: ${request.params.id}`);
});
productRouter.use('/', function (request, response) {
  response.send('List of items');
});
app.use('/products', productRouter);
app.use('/', function (request, response) {
  response.send('home Page');
});
app.listen(3000);
