const express = require('express');
const app = express();

app.set('view engine', 'hbs');

app.use('/contact', function (request, response) {
  response.render('hb.hbs', {
    title: 'Contacts',
    email: 'email@mail.ru',
    phone: '+123',
  });
});
app.use('/', function (request, response) {
  response.send('Home Page');
});
app.listen(3000);
