const express = require('express');
const fs = require('fs');
const app = express();

app.use(function (request, response, next) {
  console.log('Middleware 1');
  let now = new Date();
  let hour = now.getHours();
  let minutes = now.getMinutes();
  let seconds = now.getSeconds();
  let data = `${hour}: ${minutes}: ${seconds} ${request.method} ${
    request.url
  } ${request.get('user-agent')}`;
  console.log(data);
  fs.appendFile('server.log', data + '\n', function () {
    next();
  });
});
app.get('/', function (request, response) {
  console.log('Route to /');
  response.sendStatus(500);
});
app.listen(3000);
