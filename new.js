const http = require('http');

let message = 'Hello, World!';
http
  .createServer(function (request, response) {
    console.log(message);
    response.end(message);
  })
  .listen(3000, '127.0.0.1', () => {
    console.log('Server started listening!');
  });

function display(data, callback) {
  var randInt = Math.random() * (10 - 1) + 1;
  var err = randInt > 5 ? new Error('error. RandInt is higher than 5') : null;

  setTimeout(function () {
    callback(err, data);
  }, 0);
}
console.log('started working');

display('data research...', function (err, data) {
  if (err) throw err;
  console.log(data);
});
console.log('Ending');
