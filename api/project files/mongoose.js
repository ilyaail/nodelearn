const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userScheme = new Schema(
  {
    name: {
      type: String,
      default: 'NoName',
      required: true,
      minlength: 3,
      maxlength: 20,
    },
    age: {
      type: Number,
      default: 0,
      required: true,
      min: 1,
      max: 100,
    },

    employee: [String],
    date: Date,
  },
  {
    versionKey: false,
  }
);

mongoose.connect('mongodb://localhost:27017/userdb', {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useFindAndModify: false,
});

const User = mongoose.model('User', userScheme);
// const user = new User({ name: 'Petro', age: 12 });
// user.save(function (err) {
//   mongoose.disconnect();

//   if (err) console.log(err);

//   console.log('Object saved', user);
// });

// User.create({ name: 'Bobik', age: 21 }, function (err, doc) {
//   mongoose.disconnect();
//   if (err) return console.log(err);
//   console.log('Object saved inside user', doc);
// });

// User.findOne({ name: 'Tom' }, function (err, docs) {
//   mongoose.disconnect();

//   if (err) return console.log(err);
//   console.log(docs);
// });

// User.deleteMany({}, function (err, docs) {
//   mongoose.disconnect();

//   if (err) return console.log(err);
//   console.log(docs);
// });

User.updateOne({ name: 'Bobik' }, { name: 'Ivan' }, function (err, docs) {
  mongoose.disconnect();

  if (err) return console.log(err);
  console.log(docs);
});
