const db = require('../database');

class userCreator {
  async create(params) {
    const result = await db.createUser(params);
    return result;
  }
}
module.exports = userCreator;
