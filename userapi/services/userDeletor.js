const db = require('../database');

class userDeletor {
  async remove(id) {
    const result = await db.removeUser(id);
    return result;
  }
}
module.exports = userDeletor;
