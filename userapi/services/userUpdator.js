const db = require('../database/');

class userUpdator {
  async update(params) {
    const result = await db.updateUser(params);
    return result;
  }
}

module.exports = userUpdator;
