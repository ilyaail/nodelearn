const db = require('../database');

class userGetter {
  async gets() {
    const result = await db.findUser();
    return result;
  }
}
module.exports = userGetter;
