const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
const usersRouter = require('./routes/usersRouter');
const db = require('./database');

db.createOrOpenDatabase()
  .then(() => {
    console.log('Database was connected');
    app.listen(3000, () => {
      console.log(`Server started listen 3000 port...`);
    });
  })
  .catch((e) => {
    console.error(e);
    process.exit(-1);
  });

app.use('/users', usersRouter);
