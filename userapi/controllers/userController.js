module.exports = { getAllUsers, createUser, removeUser, updateUser };

const userGetter = require('../services/userGetter');
const userCreator = require('../services/userCreator');
const userDeletor = require('../services/userDeletor');
const userUpdator = require('../services/userUpdator');

async function getAllUsers(req, res) {
  try {
    const getter = new userGetter();
    const response = await getter.gets();
    res.send(202, response);
  } catch (error) {
    console.log(error);
  }
}

async function createUser(req, res) {
  try {
    const creator = new userCreator();

    const response = await creator.create(req.body);
    res.send(202, response);
  } catch (error) {
    console.log(error);
  }
}

async function removeUser(req, res) {
  try {
    const deletor = new userDeletor();

    const response = await deletor.remove(req.body);
    console.log(response);
    res.send(202, response);
  } catch (error) {
    console.error(error);
  }
}

async function updateUser(req, res) {
  try {
    const updater = new userUpdator();
    const response = await updater.update(req.body);
    res.send(202, response);
  } catch (error) {
    console.log(error);
  }
}
