const mongoose = require('mongoose');
const Users = require('./Schema/index.js');
module.exports = Object.freeze({
  createOrOpenDatabase,
  createUser,
  findUser,
  updateUser,
  removeUser,
});

async function createOrOpenDatabase() {
  const url = 'mongodb://localhost:27017/usersdb';
  return mongoose.connect(url, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });
}

async function findUser() {
  const result = await Users.find();
  console.log(result);
  return result;
}
async function createUser(params) {
  console.log(params);
  const result = await Users.create(params);
  console.log(result);
  if (!result) {
    return null;
  }
  return result;
}

async function removeUser(_id) {
  return Users.deleteOne({ _id });
}

async function updateUser(params) {
  const result = Users.findOneAndUpdate({ _id: params.id }, params);
  if (!result) {
    return null;
  }
  return result;
}
