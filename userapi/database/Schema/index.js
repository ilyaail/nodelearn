const mongoose = require('mongoose');

const Schema = mongoose.Schema;
// установка схемы
const userSchema = new Schema({
  name: String,
  age: Number,
});

module.exports = mongoose.model('Users', userSchema);
