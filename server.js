const { response } = require('express');
const http = require('http');

http
  .createServer(function (request, response) {
    response.setHeader('Content-Type', 'text/html; charset=utf-8;');
    if (request.url === '/') {
      response.statusCode = 302;
      response.setHeader('Location', '/newapage');
    } else if (request.url === '/newapage') {
      response.write('New Address');
    } else {
      response.statusCode = 404;
      response.write('Not Found');
    }
    response.end();
  })
  .listen(3000);
