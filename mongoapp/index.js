const MongoClient = require('mongodb').MongoClient;

const mongoClient = new MongoClient('mongodb://localhost:27017', {
  useUnifiedTopology: true,
});
mongoClient.connect(function (err, client) {
  if (err) {
    return console.log(err);
  }
  client.db('users');
  client.close();
});
