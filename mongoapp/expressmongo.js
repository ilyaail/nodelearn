const express = require('express');
const MongoClient = require('mongodb').MongoClient;
const objectId = require('mongodb').ObjectId;

const app = express();
const jsonParser = express.json();

const mongoClient = new MongoClient('mongodb://localhost:27017/', {
  useUnifiedTopology: true,
});

let dbClient;

app.use(express.static(__dirname + '/public'));

mongoClient.connect(function (err, client) {
  if (err) return console.log(err);
  dbClient = client;
  app.locals.collection = client.db('userdb').collection('users');
  app.listen(3000, function () {
    console.log('Server is waiting for connect');
  });
});
