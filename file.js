const fs = require('fs');

fs.readFile('hello.txt', 'utf8', function (error, data) {
  console.log('Async file reading ');
  if (error) throw error;
  console.log(data);
});
console.log('Syncro file reading');

let fileContent = fs.readFileSync('hello.txt', 'utf8');
console.log(fileContent);

fs.appendFileSync('hello.txt', 'haha not hello\n');

// fs.unlink('hello.txt', (err) => {
//   if (err) throw err;
//   console.log('File Deleted');
// });
