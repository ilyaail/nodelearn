const http = require('http');
const fs = require('fs');

http
  .createServer(function (request, response) {
    console.log(`requested address: ${request.url}`);
    const filePath = request.url.substr(1);
    fs.readFile(filePath, function (error, data) {
      if (error) {
        response.statusCode = 404;
        response.end('Resource not found!');
      } else {
        response.end(data);
      }
    });
  })
  .listen(3000, function () {
    console.log('server started at 3000');
  });
